// Check first whether the letter is a single character.
// If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
// If letter is invalid, return undefined.

function countLetter(letter, sentence) {
  // Check first whether the letter is a single character.
  if (typeof letter !== 'string' || letter.length !== 1) {
    // If letter is not a single character, return undefined.
    return undefined;
  }

  let count = 0;
  // Loop through the sentence and count occurrences of the letter.
  for (let i = 0; i < sentence.length; i++) {
    if (sentence.charAt(i) === letter) {
      count++;
    }
  }

  return count;
}

// An isogram is a word where there are no repeating letters.
// The function should disregard text casing before doing anything else.
// If the function finds a repeating letter, return false. Otherwise, return true.
// convert the text to lowercase to disregard text casing
function isIsogram(text) {
  const lowercaseText = text.toLowerCase();

  // create an object to store the frequency of each letter in the text
  const letterFrequency = {};

  // iterate through each letter in the text
  for (let i = 0; i < lowercaseText.length; i++) {
    const letter = lowercaseText[i];

    // if the letter has already been seen, return false
    if (letterFrequency[letter]) {
      return false;
    }

    // otherwise, add the letter to the letter frequency object
    letterFrequency[letter] = 1;
  }

  // if we've iterated through the entire text without finding any repeating letters, return true
  return true;
}

// Return undefined for people aged below 13.
// Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
// Return the rounded off price for people aged 22 to 64.
// The returned value should be a string.
function purchase(age, price) {
  let discountedPrice;
  if ((age >= 13 && age <= 21) || age >= 65) {
    // apply 20% discount for students aged 13 to 21 and senior citizens
    discountedPrice = price * 0.8;
  } else if (age >= 22 && age <= 64) {
    discountedPrice = price;
  } else {
    // return undefined for ages below 13
    return undefined;
  }
  // round off the discounted price and return it as a string
  return Math.round(discountedPrice * 100) / 100 + '';
}

function findHotCategories(items) {
  // Find categories that has no more stocks.
  // The hot categories must be unique; no repeating categories.
  // The passed items array from the test are the following:
  // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
  // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
  // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
  // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
  // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
  // The expected output after processing the items array is ['toiletries', 'gadgets'].
  // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
  const categories = {};
  for (let item of items) {
    if (item.stocks === 0) {
      categories[item.category] = true;
    }
  }
  return Object.keys(categories);
}

// Find voters who voted for both candidate A and candidate B.
// The passed values from the test are the following:
// candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
// candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
// The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
// Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
function findFlyingVoters(candidateA, candidateB) {
  const flyingVoters = [];

  for (const voter of candidateA) {
    if (candidateB.includes(voter)) {
      flyingVoters.push(voter);
    }
  }

  return flyingVoters;
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
